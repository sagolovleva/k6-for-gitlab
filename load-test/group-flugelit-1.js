'use strict'

import http from 'k6/http';
import { sleep, check, group } from 'k6';

import {
  arrayURL,
  pathWeDo,
  pathWeAre,
  tabsWeDo,
  tabsWeAre,
  allURLs } from './module-urls.js'

// this is remained here for demonstration purpose - to see the target domain
const mainURL = 'https://flugel.it';


const allVUs = tabsWeDo.length + tabsWeAre.length; // 8 VUs
// const allVUs = (tabsWeDo.length + tabsWeAre.length) * 2; // 16 VUs
// const allVUs = (tabsWeDo.length + tabsWeAre.length) * 3; // 24 VUs

export let options = {
    vus: allVUs,
    duration: '10s'
};

let resWeDo, resWeAre;

export default function() {
  group('Message - checking the Inclusiva', function() {

    group('GET requests for FLUGEL\'s endpoints - \'what we do\' ', function() {
      for (let i = 0; i < tabsWeDo.length; i++) {
      resWeDo = http.get(`${mainURL}/${pathWeDo}/${tabsWeDo[i]}`);
    }
      check(resWeDo, {
        'Status for endpoints \'What We Do\' - 200': (r) => r.status === 200,
      });
    });

    group('GET requests for FLUGEL\'s endpoints - \'who we are\' ', function() {
      for (let i = 0; i < tabsWeAre.length; i++) {
      resWeAre = http.get(`${mainURL}/${pathWeAre}/${tabsWeAre[i]}`);
    }
      check(resWeAre, {
        'Status for endpoints \'Who We Are\' - 200': (r) => r.status === 200,
      });
    })

  });

  sleep(2);
};
