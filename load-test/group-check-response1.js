// first and simple - just checking the response code
// note: it's necessary to multiply the number of VUs regard to number of (different) endpoints
'use strict'

// object for doing request(s)
import http from 'k6/http';
import { sleep, check, group } from 'k6';

let mainURL = 'https://inclusiva.io';
let appendURL1 = 'https://flugel.it';
let appendURL2 = 'https://trans-ti.com';

// export let options = {
//     vus: 10,
//     duration: '10s'
// };

export let options = {
    vus: 80,
    duration: '2m'
};

// export let options = {
//   vus: 50,
//   stages: [
//       { duration: '30s', target: 10 },
//       { duration: '1m', target: 40 },
//       { duration: '30s', target: 0 }
//   ],
// };

export default function() {
  group('Message - checking the Inclusiva', function() {

    group('GET request for endpoint - Inclusiva', function() {
      let resMain = http.get(mainURL);
      check(resMain, {
        'Response status is 200': (r) => r.status === 200,
      });
    });

    group('GET request for endpoint - Trans-TI', function() {
      let respAppend = http.get(appendURL2);
      check(respAppend, {
        'Response status is 200': (r) => r.status === 200,
      });
    })

  });

  sleep(1);
};
