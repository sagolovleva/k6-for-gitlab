'use strict'

// for "./load-test/check-loop.js"
const arrayURL = [
  'https://inclusiva.io',
  'https://flugel.it',
  'https://trans-ti.com',
  'https://test-api.k6.io/'
];

// for files:
// "./k6-for-gitlab/load-test/group-flugelit-1.js"
// "./k6-for-gitlab/load-test/group-flugelit-1.js" 

const pathWeDo = 'what-we-do';
const pathWeAre = 'who-we-are';

const tabsWeDo = [
  'overview',
  'cloud-engineering',
  'managed-services',
  'staff-augmentation',
  'flugelflow-methodology'
];
const tabsWeAre = [
  'about-us',
  'leadership-team',
  'join-our-team'
];

let allURLs = [
  'https://flugel.it/what-we-do/overview',
  'https://flugel.it/what-we-do/cloud-engineering',
  'https://flugel.it/what-we-do/managed-services',
  'https://flugel.it/what-we-do/staff-augmentation',
  'https://flugel.it/what-we-do/flugelflow-methodology',
  'https://flugel.it/who-we-are/about-us',
  'https://flugel.it/who-we-are/leadership-team',
  'https://flugel.it/who-we-are/join-our-team'
];

module.exports = {
  arrayURL,
  pathWeDo,
  pathWeAre,
  tabsWeDo,
  tabsWeAre,
  allURLs,
}
