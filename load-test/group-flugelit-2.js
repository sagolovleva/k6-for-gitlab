'use strict'

import http from 'k6/http';
import { sleep, check, group } from 'k6';
import { parseHTML } from 'k6/html';

import {
  arrayURL,
  pathWeDo,
  pathWeAre,
  tabsWeDo,
  tabsWeAre,
  allURLs } from './module-urls.js'

// this is remained here for demonstration purpose - to see the target domain
const mainURL = 'https://flugel.it';


const allVUs = tabsWeDo.length + tabsWeAre.length; // 8 VUs
// const allVUs = (tabsWeDo.length + tabsWeAre.length) * 2; // 16 VUs
// const allVUs = (tabsWeDo.length + tabsWeAre.length) * 3; // 24 VUs

export let options = {
    vus: allVUs,
    duration: '10s'
};


export default function() {

let result, doc, pageTitle;

    for (let i = 0; i < allURLs.length; i++) {
      result = http.get(allURLs[i]);

// these are needed to see that the app requests to all provided URLs
      doc = parseHTML(result.body);
      pageTitle = doc.find('head title').text();
      console.log(pageTitle);
      // console.log(result.status);
      // console.log(result.url);

      check(result, {
        'HTTP response code is 200' : (result) => result.status == 200,
      })
    }

    sleep(2);
};
