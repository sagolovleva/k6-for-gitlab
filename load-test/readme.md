# Heading 1 - brief files description

[[_TOC_]]

## Overall notes

1. This folder comprise the simple files of loading testing in overall sense. Just methods that might be applying for performance testing with defined goal(s).
2. Minimal requirements and manner to run test files are described in `readme` of the main directory.

## Outline the testing goals and/or scenario(s)
1. To see overall capacity of the tested resource we can primarily run file: `outline.js`, and we can see **k6**'s metrics:      

![k6's output of "outline.js"](./screenshot-outline-js.png "outline.js output")

2. At the `outline.js` we have as `options` - 30 VUs ans duration: 30 seconds those  set down the amount (undefined) of requests' iterations and the time the requests will be done.    

3. It is better (from my point of view) to run this file without "VUs" and "duration" in the first place, and then with {vus: 30,duration: '30s'}, and more than that to make several executions:
- to be able compare output's results - mostly, timing;
- and secondly, some domains get better results after 3rd file's execution.

4. The latter results and comparing we have to bear in mind for setting `thresholds` in the test files.      


## Further overall and brief descriptions

### Collecting several URLs in one file: `module-urls.js`

1. That is used for file(s) which make requests for a number of URLs at the time (at once):
- `check-loop.js` - making multiple requests ({vus: 20,duration: '5m'}) for random URLs;
- `group-flugelit-1.js` - using `group` for making multiple requests to **`flugel.it`** internal links;
- `group-flugelit-2.js` - multiple requests to **`flugel.it`** internal links, but looping over an array with complete URLs.

2. The idea is to get rid test file from "strings" and "arrays" of URLS. Also this might be used as module for more complex test API.

### Group-1 and Group-2

#### Files `group-flugelit-1.js` and `group-flugelit-2.js`

1. Primarily - to construct the `group` - feature when we can grouping reaquest.
2. Multiplying VUs had given multiple errors with `group`, and was created another file `group-flugelit-2.js` where is only loop over the array of the same URLs (in `group-flugelit-1.js` the URLs assembly on the fly on each group from different parts - paths).
3. Once again - both files give error of `code 200` and requesting the endpoints (return code 429), when there are:
 - 16 VUs, duration: 10s (`allVUs = (tabsWeDo.length + tabsWeAre.length) * 2;`);
 - 24 VUs, duration: 10s (`allVUs = (tabsWeDo.length + tabsWeAre.length) * 3;`).
4. There are no errors, when:
- executing both files by default - without defining VUs and duration;
 - 8 VUs, duration: 10s (`allVUs = (tabsWeDo.length + tabsWeAre.length);`).
