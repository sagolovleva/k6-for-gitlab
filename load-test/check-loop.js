'use strict'

import http from 'k6/http';
import { sleep, check } from 'k6';
import { parseHTML } from 'k6/html';
import { arrayURL } from './module-urls.js';

export let options = {
    vus: 20,
    duration: '5m'
};

export default function() {

let result, doc, pageTitle;

    for (let i = 0; i < arrayURL.length; i++) {
      result = http.get(arrayURL[i]);
      doc = parseHTML(result.body);
      pageTitle = doc.find('head title').text();
// "parseHTML"-object,2 lines above, and console.log() below are needed
// to see the suite does request to all provided URLs
      console.log(pageTitle);
      
      check(result, {
        'HTTP response code is 200' : (result) => result.status == 200,
      })
  }
    sleep(1);
};
