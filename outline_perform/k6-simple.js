'use strict'

// object for doing request(s)
import http from 'k6/http';
import { sleep } from 'k6';

let mainURL = 'https://inclusiva.io';


// Entry 0 - gets "http_req_duration":
// avg=473.95ms min=223ms    med=491.02ms max=862.93ms p(90)=778.16ms p(95)=827.14ms
export let options = {
    vus: 10,
    duration: '10s'
};

export default function() {
    http.get(mainURL);
    sleep(1);
};
