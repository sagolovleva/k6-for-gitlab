// only: ramp up and down and check() the response code;

'use strict'

import http from 'k6/http';
import { sleep, check } from 'k6';

let mainURL = 'https://inclusiva.io';

// Up to 50 looping VUs for 38m0s over 7 stages (gracefulRampDown: 30s, gracefulStop: 30s)
export let options = {
  stages: [
      { duration: '5m', target: 30 }, // simulate ramp-up of traffic from 1 to 30 users over 5 minutes.
      { duration: '10m', target: 30 }, // stay at 30 users for 10 minutes
      { duration: '3m', target: 50 }, // ramp-up to 50 users over 3 minutes (peak hour starts)
      { duration: '2m', target: 50 }, // stay at 50 users for short amount of time (peak hour)
      { duration: '3m', target: 30 }, // ramp-down to 30 users over 3 minutes (peak hour ends)
      { duration: '10m', target: 30 }, // continue at 30 for additional 10 minutes
      { duration: '5m', target: 0 } // ramp-down to 0 users
  ],
};

export default function() {
    let resp = http.get(mainURL);

    check(resp, {
    'Response status is 200': (resp) => resp.status === 200,
    });

    sleep(1);
};
