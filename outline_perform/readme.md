# Tests for defining the performance limits

[[_TOC_]]

## Outline performance explanations

1. Main `k6`'s tools - VUs (virtual users), iterating requests (iterations) and time during which the VUs make requests through iterations - test's parameters. 

2. The first and primer goal is (was) to define a capacity (response timing, VUs for load testing, durations) of the testing (in this case) website: `https://inclusiva.io` - to get known of values that we can use for load testing and those for stress testing, etc.

3. Usually, starting values might be set from expections of website (or API, etc.) performance.

4. In this case primary have been made a number of launches the test file: 
- [`check-resp1.js`](https://gitlab.com/chloetara/k6-for-gitlab/-/blob/master/outline_perform/check-resp1.js).    
The results of those launches might be used to set `thresholds` for example or continue just check out the response timing.

5.  Also results from `check-resp1.js` was the base to define `stages` ([take a look](https://gitlab.com/chloetara/k6-for-gitlab/-/blob/master/outline_perform/check-resp1-ramp1.js#L11)) in the following test file:
- [`check-resp1-ramp1.js`](https://gitlab.com/chloetara/k6-for-gitlab/-/blob/master/outline_perform/check-resp1-ramp1.js).     
That contain a slightly more complex scenario - changing VUs and durations over 38 minutes (in this particular case-file). 

### - test file: check-resp1.js

1. Simple `k6`- test file where have been used main parameters: `VUs` - or `vus` are a quantity of virtual users; and `duration` - a time during which VUs are making iterating requests for the tested endpoint.  
2. There were series of launches this file by changing quantity of VUs and (time) durations of iterations.   
3. At the beginning the most criterion is response timing - in the terms of `k6` is a `http_req_duration` - to define `check()` and/or `thresholds`. 

#### Results of check-resp1.js (response timing only):

> response timing of Entry 1 - vus: 30, duration: 30s - 1 launch:    
- avg=823.88ms ---min=220.78ms ---med=591.03ms ---max=5.96s    ---p(90)=1.56s    ---p(95)=1.92s      

> response timing of Entry 2 - vus: 30, duration: 1m - 1 launch:   
- avg=755.21ms ---min=229.29ms ---med=558.12ms ---max=5.87s    ---p(90)=1.16s    ---p(95)=2.11s      

> response timing of Entry 3 - vus: 30, duration: 3m - 3 launches sequently:

- avg=568.42ms ---min=237.61ms ---med=477.71ms ---max=6.33s    ---p(90)=772.14ms ---p(95)=940.09ms         
- avg=487.76ms ---min=218.2ms  ---med=399.37ms ---max=6.22s    p(90)=685.63ms p(95)=834.26ms         
- avg=525.81ms ---min=238.82ms ---med=447.97ms ---max=4.81s    ---p(90)=747.7ms  ---p(95)=843.91ms         

> response timing of Entry 4 - vus: 40, duration: 5m - 3 launches sequently:

- avg=744.76ms ---min=253.09ms ---med=675.66ms ---max=8.68s    ---p(90)=1.05s    ---p(95)=1.21s     
- avg=731.66ms ---min=242.57ms ---med=733.57ms ---max=6.09s  ---p(90)=1.01s    ---p(95)=1.16s     
- avg=766.65ms ---min=225.91ms ---med=761.42ms ---max=6.84s    ---p(90)=1.04s    ---p(95)=1.18s     

> response timing of Entry 5 - vus: 50, duration: 10m - 4 launches sequently:
- avg=995.53ms ---min=230.04ms ---med=1s       ---max=8.18s    ---p(90)=1.36s    ---p(95)=1.54s     
- avg=1.02s    ---min=223.01ms ---med=1.03s    ---max=8.5s   ---p(90)=1.37s    ---p(95)=1.54s     
- avg=991.88ms ---min=248.99ms ---med=1.01s    ---max=5.98s  ---p(90)=1.3s     ---p(95)=1.46s     
- avg=975.25ms ---min=161.2ms  ---med=1.02s    ---max=7.07s  ---p(90)=1.28s    ---p(95)=1.44s     

### - test file: check-resp1-ramp1.js

#### Stages in check-resp1-ramp1.js:
> { duration: '5m', target: 30 }      
{ duration: '10m', target: 30 }     
{ duration: '10m', target: 30 }     
{ duration: '3m', target: 50 }    
{ duration: '2m', target: 50 }      
{ duration: '3m', target: 30 }    
{ duration: '10m', target: 30 }    
{ duration: '5m', target: 0 }

1. All report of launching ` check-resp1-ramp1.js` see [here](https://gitlab.com/chloetara/k6-for-gitlab/-/blob/master/outline_perform/report-ramp1.md).