'use strict'


import http from 'k6/http';
import { sleep, check, group } from 'k6';
import { Rate } from 'k6/metrics';

import {
    arrayURL,
    pathWeDo,
    pathWeAre,
    tabsWeDo,
    tabsWeAre,
    allURLs } from '../load-test/module-urls.js'

// this is remained here for demonstration purpose - to see the target domain
const mainURL = 'https://flugel.it';

let failRate = new Rate ('failed_requests');

export let options = {
    discardResponseBodies: true,
    
    scenarios: {
      overall_scenario: {
        executor: 'per-vu-iterations',
        // vus: 20,
        vus: 10,
        // iterations: 30,
        iterations: 10,

        maxDuration: '2m',
      },
    },

    thresholds: {
        failed_requests: ['rate<=0'],
        http_req_duration: ['p(95)<900'],
        http_req_duration: ['p(90)<500'],
    }
  };
  
let resWeDo, resWeAre;

  export default function() {
    group('Message - checking the \'fluge\.it\' ', function() {
  
      group('GET requests for FLUGEL\'s endpoints - \'what we do\' ', function() {
        for (let i = 0; i < tabsWeDo.length; i++) {
        resWeDo = http.get(`${mainURL}/${pathWeDo}/${tabsWeDo[i]}`);
      }
        check(resWeDo, {
          'Status for endpoints \'What We Do\' - 200': (r) => r.status === 200,
        });

        failRate.add(resWeDo.status !== 200);

        sleep(1);
      });
  
      group('GET requests for FLUGEL\'s endpoints - \'who we are\' ', function() {
        for (let i = 0; i < tabsWeAre.length; i++) {
        resWeAre = http.get(`${mainURL}/${pathWeAre}/${tabsWeAre[i]}`);
      }
        check(resWeAre, {
          'Status for endpoints \'Who We Are\' - 200': (r) => r.status === 200,
        });

        failRate.add(resWeAre.status !== 200);

        sleep(1);
      });
  
      
    });
  
    // sleep(1);
  };