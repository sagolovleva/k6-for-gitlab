'use strict'

import http from 'k6/http';
import { sleep, check } from 'k6';
import { Rate } from 'k6/metrics';

// let mainURL = 'https://inclusiva.io';
const mainURL = 'https://flugel.it';

const failRate = new Rate ('failed_requests');

export let options = {
    discardResponseBodies: true,
    
    scenarios: {
      mainPage: {
        executor: 'per-vu-iterations',
        vus: 20,
        iterations: 30,
        maxDuration: '2m',
      },
    },
  thresholds: {
    failed_requests: ['rate<=0'],
    http_req_duration: ['p(95)<900'],
    http_req_duration: ['p(90)<600'],
  }  
};

export default function() {
    
    let result = http.get(mainURL);

    check(result, {
        'HTTP response code is 200' : (r) => r.status === 200,
    });

    failRate.add(result.status !== 200);

    sleep(1);
};