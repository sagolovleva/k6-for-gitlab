'use strict'

import http from 'k6/http';
import { sleep, check } from 'k6';

let mainURL = 'https://inclusiva.io';

export let options = {
    discardResponseBodies: true,
    
    scenarios: {
      mainPageInclusiva: {
        executor: 'per-vu-iterations',
        vus: 10,
        iterations: 20,
        maxDuration: '2m',
      },
    },
};

export default function() {
    
    let result = http.get(mainURL);

    check(result, {
        'HTTP response code is 200' : (r) => r.status === 200,
    })

    sleep(1);
};